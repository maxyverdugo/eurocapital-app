import { isIOS } from "tns-core-modules/platform";
import { EventData } from "tns-core-modules/data/observable";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import { AppRootViewModel } from "./app-root-view-model";

export function drawerLoaded(args: EventData) {
    let drawer = <RadSideDrawer>args.object;

    if (isIOS) {
        // if your menu is drawn 'below' the hostview, do this:
        //drawer.ios.defaultSideDrawer.style.shadowMode = 1; // TKSideDrawerShadowMode.Hostview;
        drawer.ios.defaultSideDrawer.style.shadowMode = 2; // TKSideDrawerShadowMode.SideDrawer;
        // if you have shadowMode = 2, then you can add a little dim to the lower layer to add some depth. Keep it subtle though:
        drawer.ios.defaultSideDrawer.style.dimOpacity = 0.12;
        // then tweak the shadow to your liking:
        drawer.ios.defaultSideDrawer.style.shadowOpacity = 0.75; // 0-1, higher is darker
        drawer.ios.defaultSideDrawer.style.shadowRadius = 5; // higher is more spread
        // bonus feature: control the menu animation speed (in seconds)
        drawer.ios.defaultSideDrawer.transitionDuration = 0.25;
    }

    drawer.bindingContext = new AppRootViewModel();
}
