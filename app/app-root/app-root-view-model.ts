import { Observable } from 'data/observable';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { topmost } from "tns-core-modules/ui/frame";
import * as application from "application";

export class AppRootViewModel extends Observable {

    options: { index: number, name: string }[] = [
        { index: 0, name: "Salir" }
    ];

    chooseOption(args): void {
        var obj = this.options[args.index];
        /*this.set("city", obj.city);
        this.set("state", obj.state);
        this.set("temp", obj.temp);
        this.set("img", obj.img);*/
        this.logout();
        const sideDrawer = <RadSideDrawer>application.getRootView();
        sideDrawer.toggleDrawerState();
    }

    logout() {
        topmost().navigate({
            moduleName: "/login/login-page",
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }

    /*city = this.cities[0].city;
    state = this.cities[0].state;
    temp = this.cities[0].temp;
    img = this.cities[0].img;
*/
    constructor() {
        super();
    }
}
