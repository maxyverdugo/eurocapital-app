import { Observable } from "data/observable";

import { Credito } from "../shared/credito-model";

/* ***********************************************************
* This is the item details view model.
*************************************************************/
export class CreditoDetailViewModel extends Observable {
    constructor(private _credito: Credito) {
        super();
    }

    get credito(): Credito {
        return this._credito;
    }
}
