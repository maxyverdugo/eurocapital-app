import { Observable } from "data/observable";
import { ObservableArray } from "data/observable-array";
import { Feedback, FeedbackPosition, FeedbackType } from "nativescript-feedback";
import { ModalDatetimepicker, PickerOptions } from "nativescript-modal-datetimepicker";
import { topmost } from "tns-core-modules/ui/frame";
import * as dialogs from "ui/dialogs";
import { Label } from "ui/label";
import { NavigatedData, Page } from "ui/page";
import { ScrollView } from "ui/scroll-view";
import { Switch } from "ui/switch";
import { TextField } from "ui/text-field";
import { Login } from "../login/shared/login-model";
import { Config } from "../shared/config";
import { ObservableProperty } from "../shared/observable-property-decorator";
import { Credito } from "./shared/credito-model";
import { RootObject } from "./shared/credito-model-response";
import { CreditoService } from "./shared/credito-service";
import { RutValidation } from "../assets/validations/rut";
const platformModule = require("tns-core-modules/platform");

/* ***********************************************************
* This is the master list view model.
*************************************************************/
export class CreditosListViewModel extends Observable {
    date: string;
    time: string;
    @ObservableProperty() creditoModel: Credito;
    @ObservableProperty() isLoading: boolean = false;
    private modalDatetimepicker: ModalDatetimepicker;
    private _creditoService: CreditoService;
    private _rutValidation: RutValidation;
    private feedback: Feedback;

    constructor(private user: Login) {
        super();
        this.creditoModel = new Credito({ id: user.id, login: user.rut });
        this._creditoService = CreditoService.getInstance();
        this._rutValidation = new RutValidation();
        this.modalDatetimepicker = new ModalDatetimepicker();
        this.feedback = new Feedback();

    }

    load(): void {
        this.isLoading = false;
    }

    showPicker(): void {
        const page = topmost().currentPage;
        const tag = "car";
        const selectedValue = this.creditoModel.plazoCredito;
        const tipoCredito = this.creditoModel.tipoCuota;
        const context = { tag, selectedValue, tipoCredito }; // añadir tipo de credito
        page.showModal("/credito/modal/list-selector-modal-page", context, (arg) => {
            if (arg != null) {
                const lblImagen = <Label>page.getViewById("plazoCredito");
                lblImagen.text = arg;
                this.creditoModel.plazoCredito = arg.toString();
                this.creditoModel.plazo = parseInt(this.creditoModel.plazoCredito.replace(" Meses", ""), 0);
            }
            console.log("Cerro" + arg);
        }, false, true, true);
    }
    showPeriod(): void {
        const page = topmost().currentPage;
        const tag = "seats";
        const selectedValue = this.creditoModel.primerVencimiento;
        const context = { tag, selectedValue };
        page.showModal("/credito/modal/list-selector-modal-page", context, (arg) => {
            if (arg != null) {
                const lblImagen = <Label>page.getViewById("primerVencimiento");
                this.creditoModel.primerVencimiento = arg;
            }
            console.log("Cerro" + arg);
        }, false, true, true);
    }

    logout() {
        topmost().navigate({
            moduleName: "/login/login-page",
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }

    onBlur(args) {
        // blur event will be triggered when the user leaves the TextField
        const textField = <TextField>args.object;
        console.log(textField.id)
        if (textField.id == "valorVehiculo" || textField.id == "montoPie") {
            const nuevovalor = textField.text;
            if (nuevovalor === "") {
                return false;
            }
            const valor: number = parseInt(nuevovalor.replace(/\./g, ""), 0);
            const newvalor = valor.toString().replace(/(.)(?=(\d{3})+$)/g, "$1.").toString();
            textField.text = newvalor;
        } else {
            textField.text = this.formateaRut(textField.text)
        }
    }

    onRut(args) {
        const textField = <TextField>args.object;
        console.log(textField.text);
        textField.text = this.formateaRut(textField.text);
    }
    formateaRut(rut) {
        let rutPuntos = "";
        let actual = rut.replace(/^0+/, "");
        if (actual != '' && actual.length > 1) {
            let sinPuntos = actual.replace(/\./g, "");
            let actualLimpio = sinPuntos.replace(/-/g, "");
            let inicio = actualLimpio.substring(0, actualLimpio.length - 1);
            rutPuntos = "";
            let i = 0;
            let j = 1;
            for (i = inicio.length - 1; i >= 0; i--) {
                let letra = inicio.charAt(i);
                rutPuntos = letra + rutPuntos;
                if (j % 3 == 0 && j <= inicio.length - 1) {
                    rutPuntos = "." + rutPuntos;
                }
                j++;
            }
            let dv = actualLimpio.substring(actualLimpio.length - 1);
            rutPuntos = rutPuntos + "-" + dv;
        }
        return rutPuntos;
    }
    onEditButtonTap() {
        /** añadir validacion rut */
        let rut = <TextField>topmost().getViewById('rut');
        if (rut.text == null || rut.text == "") {
            if (platformModule.iOS) {
                this.feedback.error({
                    message: "Debes ingresar el rut",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar el rut",
                    okButtonText: "Aceptar"
                });
            }
            return;
        }
        if (!this._rutValidation.validar(rut.text)) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar un Rut válido",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar un Rut válido",
                    okButtonText: "Aceptar"
                });
            }
            return;
        }
        if (this.creditoModel.valorVehiculo == null || this.creditoModel.valorVehiculo === 0) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar el valor del vehículo",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar el valor del vehículo",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }
            // alert("Debes ingresar el valor del vehículo");

            return;
        }
        if (this.creditoModel.montoPie == null || this.creditoModel.montoPie === 0) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar el monto del pie",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar el monto del pie",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }
            // alert("Debes ingresar el monto del pie");

            return;
        }

        console.log("montoPie " + this.creditoModel.montoPie.toString().replace(/\./g, ""));
        if (parseInt(this.creditoModel.montoPie.toString().replace(/\./g, ""), 0) >=
            parseInt(this.creditoModel.valorVehiculo.toString().replace(/\./g, ""), 0)) {
            // alert("El valor del pie debe ser menor al valor del vehículo");
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "El valor del pie debe ser menor al valor del vehículo",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "El valor del pie debe ser menor al valor del vehículo",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }

            return;
        }

        const page = topmost().currentPage;
        const mySwitch = <Switch>page.getViewById("swindemnizacion");
        this.creditoModel.indemnizacion = "NO";
        console.log("checked" + mySwitch.checked);
        if (mySwitch.checked) {
            this.creditoModel.indemnizacion = "SI";
        }

        this.isLoading = true;
        this._creditoService.simular(this.creditoModel)
            .then((credito: RootObject) => {
                if (credito.SimulacionCreditoResult != null
                    && credito.SimulacionCreditoResult.RespuestaSimulacion.CodigoError === "0") {
                    this.isLoading = false;
                    console.log(credito)
                    this.navigateHome(credito);
                } else {
                    this.isLoading = false;
                    if (platformModule.isIOS) {
                        this.feedback.warning({
                            message: "Error al generar la simulación de crédito"
                        });
                    } else {
                        dialogs.alert({
                            title: "Error",
                            message: "Error al generar la simulación de crédito",
                            okButtonText: "Aceptar"
                        }).then(() => {
                            console.log("Dialog closed!");
                        });
                    }
                    // alert("Error al generar la simulación de crédito.");
                }
            })
            .catch((error) => {
                this.isLoading = false;
                if (platformModule.isIOS) {
                    this.feedback.warning({
                        message: "Error de conexión al servicio. Usuario no posee privilegios."
                    });
                } else {
                    dialogs.alert({
                        title: "Error",
                        message: "Error de conexión al servicio. Usuario no posee privilegios.",
                        okButtonText: "Aceptar"
                    }).then(() => {
                        console.log("Dialog closed!");
                    });
                }
                // alert("Error de conexión al servicio. Usuario no posee privilegios.");
                console.log("error: " + error);
            });
    }

    selectConvencional(args): void {
        const page = topmost().currentPage;
        let lblImagen = page.getViewById("lblConvencionalImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref font-selected";
        lblImagen = page.getViewById("lblConvencionalTexto");
        lblImagen.className = "texto-infoseleccion font-selected";
        this.creditoModel.tipoCuota = "CF";
        lblImagen = page.getViewById("lblInteligenteImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref";
        lblImagen = page.getViewById("lblInteligenteTexto");
        lblImagen.className = "texto-infoseleccion";
    }

    selectInteligente(args): void {
        const page = topmost().currentPage;
        let lblImagen = page.getViewById("lblConvencionalImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref";
        lblImagen = page.getViewById("lblConvencionalTexto");
        lblImagen.className = "texto-infoseleccion";

        lblImagen = page.getViewById("lblInteligenteImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref font-selected";
        lblImagen = page.getViewById("lblInteligenteTexto");
        lblImagen.className = "texto-infoseleccion font-selected";
        this.creditoModel.tipoCuota = "CI";
        this.creditoModel.plazoCredito = "24 Meses";
        this.creditoModel.plazo = 24;
    }
    selectNuevo(args): void {
        const page = topmost().currentPage;
        let lblImagen = page.getViewById("lblUsadoImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref";
        lblImagen = page.getViewById("lblUsadoTexto");
        lblImagen.className = "texto-infoseleccion";
        this.creditoModel.tipoVehiculo = "NU";
        lblImagen = page.getViewById("lblNuevoImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref font-selected";
        lblImagen = page.getViewById("lblNuevoTexto");
        lblImagen.className = "texto-infoseleccion font-selected";
    }
    selectUsado(args): void {
        const page = topmost().currentPage;
        let lblImagen = page.getViewById("lblNuevoImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref";
        lblImagen = page.getViewById("lblNuevoTexto");
        lblImagen.className = "texto-infoseleccion";

        lblImagen = page.getViewById("lblUsadoImagen");
        lblImagen.className = "fa text-muted m-r-10 m-b-10 m-y-auto icon-ref font-selected";
        lblImagen = page.getViewById("lblUsadoTexto");
        lblImagen.className = "texto-infoseleccion font-selected";
        this.creditoModel.tipoVehiculo = "US";

    }

    selectDate(args): void {
        const today = new Date();
        const mindate = new Date();
        const maxdate = new Date();
        const pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        mindate.setDate(today.getDate() + 30);
        maxdate.setDate(today.getDate() + 90);
        const dt = new Date(this.creditoModel.primerVencimiento.replace(pattern, "$3-$2-$1"));
        this.modalDatetimepicker.pickDate({
            title: "Seleccione Fecha",
            theme: "light",
            startingDate: dt,
            maxDate: maxdate,
            minDate: mindate,
            is24HourView: false
        }).then((result: any) => {
            if (result) {
                this.creditoModel.primerVencimiento = result.day + "/" + result.month + "/" + result.year;
                // this.set("date", "Date is: " + result.day + "-" + result.month + "-" + result.year);
            } else {
                // this.set("date", false);
            }
        })
            .catch((error) => {
                console.log("Error: " + error);
            });
    }

    private navigateHome(credito: RootObject) {
        const page = topmost().currentPage;
        const context = { credito };
        page.showModal("./credito/credito-edit-page/credito-edit-page", context, () => {
            console.log("Cerro");
        }, true, true);
    }
}
