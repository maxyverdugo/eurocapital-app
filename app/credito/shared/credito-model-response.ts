import { Observable } from "data/observable";
import { ObservableProperty } from "../../shared/observable-property-decorator";
export class Respuesta {
    Codigo: string;
    Descripcion: string;
}

export class RespuestaSimulacion {
    CantidadCuotas: string;
    CodigoError: string;
    ComisionDealer: string;
    DescripcionError: string;
    ImpuestoPagare: string;
    MontoSeguroCesantia: string;
    MontoSeguroDesgravamen: string;
    MontoSegurodobleIndemnizacion: string;
    Tasa: string;
    TasaCAE: string;
    TotalFinanciar: string;
    TotalRecargos: string;
    VFMG: string;
    ValorCuota: string;
    ValorCuota2: string;

    get TotalFinanciarFormato(): string {
        return "$ " + parseInt(this.TotalFinanciar).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }

    get ImpuestoPagareFormato(): string {
        return "$ " + parseInt(this.ImpuestoPagare).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
    get MontoSeguroCesantiaFormato(): string {
        return "$ " + parseInt(this.MontoSeguroCesantia).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
    get MontoSeguroDesgravamenFormato(): string {
        return "$ " + parseInt(this.MontoSeguroDesgravamen).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
    get MontoSegurodobleIndemnizacionFormato(): string {
        return "$ " + parseInt(this.MontoSegurodobleIndemnizacion).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }

    get ComisionDealerFormato(): string {
        return "$ " + parseInt(this.ComisionDealer).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
    get ValorCuotaFormato(): string {
        return "$ " + parseInt(this.ValorCuota).toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
}

export class SimulacionCreditoResult {
    Respuesta: Respuesta;
    RespuestaSimulacion: RespuestaSimulacion;
}

export class RootObject {
    SimulacionCreditoResult: SimulacionCreditoResult;
}