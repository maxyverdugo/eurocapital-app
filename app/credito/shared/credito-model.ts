import { Observable } from "data/observable";

import { ObservableProperty } from "../../shared/observable-property-decorator";

export class Credito extends Observable {
    @ObservableProperty() id: string;
    @ObservableProperty() login: string;
    @ObservableProperty() rut: string;
    @ObservableProperty() _tipoCuota: string;
    @ObservableProperty() _tipoVehiculo: string;
    @ObservableProperty() _flexible: string;
    @ObservableProperty() _valorVehiculo: number;
    @ObservableProperty() _montoPie: number;
    @ObservableProperty() _montoSolicitado: number;
    @ObservableProperty() _montoCredito: number;
    @ObservableProperty() _plazo: number;
    @ObservableProperty() _plazoCredito: string;
    @ObservableProperty() _desgravamen: string;
    @ObservableProperty() _cesantia: string;
    @ObservableProperty() _indemnizacion: string;
    @ObservableProperty() _fechaCurse: string;
    @ObservableProperty() _primerVencimiento: string;
    @ObservableProperty() _isModelValid: boolean;


    constructor(options: any) {
        super();
        this.id = options.id;
        this.login = options.login;
        this.rut = "";
        this.tipoCuota = "CF";
        this.tipoVehiculo = "NU";
        this.flexible = "NO";
        this.plazo = 48;
        this.plazoCredito = "48 Meses";
        this.primerVencimiento = this.obtenerVencimiento();
        this.desgravamen = "SI";
        this.cesantia = "SI";
        this.indemnizacion = "SI";
        this.updateDependentProperties();
    }

    get plazoCredito(): string {
        return this._plazoCredito;
    }

    set plazoCredito(value: string) {
        if (this._plazoCredito === value) {
            return;
        }
        this._plazoCredito = value;
        this.notifyPropertyChange("plazoCredito", value);
    }


    get tipoCuota(): string {
        return this._tipoCuota;
    }

    set tipoCuota(value: string) {
        if (this._tipoCuota === value) {
            return;
        }
        this._tipoCuota = value;
        this.notifyPropertyChange("tipoCuota", value);
    }

    get tipoVehiculo(): string {
        return this._tipoVehiculo;
    }

    set tipoVehiculo(value: string) {
        if (this._tipoVehiculo === value) {
            return;
        }
        this._tipoVehiculo = value;
        this.notifyPropertyChange("tipoVehiculo", value);
    }

    get flexible(): string {
        return this._flexible;
    }

    set flexible(value: string) {
        if (this._flexible === value) {
            return;
        }
        this._flexible = value;
        this.notifyPropertyChange("flexible", value);
    }

    get valorVehiculo(): number {
        return this._valorVehiculo;
    }

    set valorVehiculo(value: number) {
        if (this._valorVehiculo === value) {
            return;
        }
        this._valorVehiculo = value;
        this.notifyPropertyChange("valorVehiculo", value);
    }

    get montoPie(): number {
        return this._montoPie;
    }

    set montoPie(value: number) {
        if (this._montoPie === value) {
            return;
        }
        this._montoPie = value;
        this.notifyPropertyChange("montoPie", value);
    }

    get montoSolicitado(): number {
        return this._montoSolicitado;
    }

    set montoSolicitado(value: number) {
        if (this._montoSolicitado === value) {
            return;
        }
        this._montoSolicitado = value;
        this.notifyPropertyChange("montoSolicitado", value);
    }

    get plazo(): number {
        return this._plazo;
    }

    set plazo(value: number) {
        if (this._plazo === value) {
            return;
        }
        this._plazo = value;
        this.notifyPropertyChange("plazo", value);
    }

    get desgravamen(): string {
        return this._desgravamen;
    }

    set desgravamen(value: string) {
        if (this._desgravamen === value) {
            return;
        }
        this._desgravamen = value;
        this.notifyPropertyChange("desgravamen", value);
    }

    get cesantia(): string {
        return this._cesantia;
    }

    set cesantia(value: string) {
        if (this._cesantia === value) {
            return;
        }
        this._cesantia = value;
        this.notifyPropertyChange("cesantia", value);
    }

    get indemnizacion(): string {
        return this._indemnizacion;
    }

    set indemnizacion(value: string) {
        if (this._indemnizacion === value) {
            return;
        }
        this._indemnizacion = value;
        this.notifyPropertyChange("indemnizacion", value);
    }

    get fechaCurse(): string {
        return this._fechaCurse;
    }

    set fechaCurse(value: string) {
        if (this._fechaCurse === value) {
            return;
        }
        this._fechaCurse = value;
        this.notifyPropertyChange("fechaCurse", value);
    }

    get primerVencimiento(): string {
        return this._primerVencimiento;
    }

    set primerVencimiento(value: string) {
        if (this._primerVencimiento === value) {
            return;
        }
        this._primerVencimiento = value;
        this.notifyPropertyChange("primerVencimiento", value);
    }

    get isModelValid(): boolean {
        return this._isModelValid;
    }

    set isModelValid(value: boolean) {
        if (this._isModelValid === value) {
            return;
        }
        this._isModelValid = value;
        this.notifyPropertyChange("isModelValid", value);
    }
    private updateDependentProperties(): void { 


    }

    private obtenerVencimiento(): string
    {
        var today = new Date();
        var dd = today.getDate();
        var hoy = today.getDate();
        var mm = today.getMonth()+2;
        var dia =dd.toString();
        var mes=mm.toString();
        var yyyy = today.getFullYear();
        //si el día es menor al 25 entonces 25
        //si el día es menor al 15 entonces 15
        //de lo contrario 5
        if(hoy<25){
            dd=25;
        }
        if(hoy<20){
            dd=20;
        } 
        if(hoy<15){
            dd=15;
        } 
        if(hoy<5)
        {
            dd=5;
        }
        if(hoy>=25){
            mm++;
        }
        if(dd<10){
            dia='0'+dd;
        } 
        if(mm>12){
            yyyy++;
        }
        if(mm<10){
            mes='0'+mm;
        } 
        return dd+'/'+mes+'/'+yyyy;
    }
}
