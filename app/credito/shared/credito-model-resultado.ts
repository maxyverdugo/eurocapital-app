export class ResultadoSimulacion {
    CantidadCuotas: string;
    CodigoError: string;
    ComisionDealer: string;
    DescripcionError: string;
    ImpuestoPagare: string;
    MontoSeguroCesantia: string;
    MontoSeguroDesgravamen: string;
    MontoSegurodobleIndemnizacion: string;
    Tasa: string;
    TasaCAE: string;
    TotalFinanciar: string;
    TotalRecargos: string;
    VFMG: string;
    ValorCuota: string;
    ValorCuota2: string;
}