import { Config } from "../../shared/config";
import { Credito } from "./credito-model";
import { RootObject } from "./credito-model-response";
import { File } from "file-system";
const httpModule = require("http");

const editableProperties = [
    "class",
    "doors",
    "hasAC",
    "transmission",
    "luggage",
    "name",
    "price",
    "seats",
    "imageUrl"
];

export class CreditoService {
    static getInstance(): CreditoService {
        return CreditoService._instance;
    }

    private static _instance: CreditoService = new CreditoService();

    private static cloneUpdateModel(credito: Credito): object {
        // tslint:disable-next-line:ban-comma-operator
        return editableProperties.reduce((a, e) => (a[e] = credito[e], a), { _id: credito.id });
    }

    constructor() {
        if (CreditoService._instance) {
            throw new Error("Use CreditoService.getInstance() instead of new.");
        }

        CreditoService._instance = this;
    }

    simular(creditoModel: Credito): Promise<any> {
        const re = new RegExp(/\./, "g");
        let montoSolicitado = parseInt(creditoModel.valorVehiculo.toString()
        .replace(re,"")) - parseInt(creditoModel.montoPie.toString().replace(re,""));
        console.log("creditoModel: " + montoSolicitado + JSON.stringify(creditoModel));
        const ss = {
            simulacionCreditoModel:
                {
                    TipoCuota : creditoModel.tipoCuota,
                    TipoVehiculo : creditoModel.tipoVehiculo,
                    CiFlexible : creditoModel.flexible,
                    ValorVehiculo : creditoModel.valorVehiculo,
                    MontoSolicitado : montoSolicitado,
                    Plazo : creditoModel.plazo,
                    OpcionSeguroDesgravamen : creditoModel.desgravamen,
                    OpcionSeguroCesantia : creditoModel.cesantia,
                    OpcionSeguroDobleIndemnizacion : creditoModel.indemnizacion,
                    CuotasVariables : {},
                    FechaCurse : creditoModel.fechaCurse,
                    FechaPrimerVencimiento : creditoModel.primerVencimiento,
                    Login : creditoModel.login,
                    IdSesion : creditoModel.id,
                    RutCliente: creditoModel.rut
                }
        };
        console.log("Credito: " + JSON.stringify(ss));
        const creditoUrl = Config.urlServices + "SimulacionCredito/";

        return httpModule.request({
            url: creditoUrl,
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                simulacionCreditoModel:
                    {
                        TipoCuota : creditoModel.tipoCuota,
                        TipoVehiculo : creditoModel.tipoVehiculo,
                        CiFlexible : creditoModel.flexible,
                        ValorVehiculo : creditoModel.valorVehiculo,
                        MontoSolicitado : montoSolicitado,
                        Plazo : creditoModel.plazo,
                        OpcionSeguroDesgravamen : creditoModel.desgravamen,
                        OpcionSeguroCesantia : creditoModel.cesantia,
                        OpcionSeguroDobleIndemnizacion : creditoModel.indemnizacion,
                        CuotasVariables : {},
                        FechaCurse : creditoModel.fechaCurse,
                        FechaPrimerVencimiento : creditoModel.primerVencimiento,
                        Login : creditoModel.login,
                        IdSesion : creditoModel.id,
                        RutCliente: creditoModel.rut
                    }
            })
        }).then((response) => {
            const result = <RootObject>response.content.toJSON();
            console.log("respuesta: " + JSON.stringify(result));
            if (result.SimulacionCreditoResult.Respuesta.Codigo === "0") {
                //loginModel.id =result.SimulacionCreditoResult.RespuestaSimulacion.CodigoError;
            }

            return Promise.resolve(result);
            //vm.set("isItemVisible", true);
            //vm.set("message", result.json.username);
            // << (hide)
        }, (e) => {
            // >> (hide)
            console.log("Error: ");
            console.log(e);
            // << (hide)

            return Promise.reject(creditoModel);
        });
    }
}
