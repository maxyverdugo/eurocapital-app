import { Observable } from "data/observable";
import { topmost } from "ui/frame";
import { NavigatedData, Page } from "ui/page";
import { CreditosListViewModel } from "./credito-view-model";

let viewModel = null;

export function textChange(propertyChangeData: any) {
// console.log(propertyChangeData.propertyName + " has been changed and the new value is: " + propertyChangeData.value);
    if (propertyChangeData.value == null || propertyChangeData.value === "") {
        return;
    }
    /* alert(propertyChangeData.value.length);
    const valorDigitado = propertyChangeData.value.substring(propertyChangeData.value.length - 1,
        propertyChangeData.value.length);

    const nuevovalor = viewModel.creditoModel.valorVehiculo === null ? "" :
    viewModel.creditoModel.valorVehiculo.toString() + "" + valorDigitado;

    const valor: number = parseInt(nuevovalor.replace(/\./g, ""), 0);
    const newvalor = valor.toString().replace(/(.)(?=(\d{3})+$)/g, "$1.").toString();
    const largo = newvalor.length;
    // alert(propertyChangeData.object.id);

    if (propertyChangeData.object.id === "valorVehiculo") {
        alert(viewModel.creditoModel.valorVehiculo);
        viewModel.creditoModel.valorVehiculo = newvalor;
    } else {
        viewModel.creditoModel.montoPie = newvalor;
    } */
    // .object.text = newvalor;

    return false;

    // valor.toString().replace(/(.)(?=(\d{3})+$)/g, "$1.").toString();
    // propertyChangeData.object.nativeView.setSelection(largo);
}

export function lostFocus(propertyChangeData: any) {
        if (propertyChangeData.value == null || propertyChangeData.value === "") {
            return;
        }
        const valorDigitado = propertyChangeData.value.substring(propertyChangeData.value.length - 1,
            propertyChangeData.value.length);

        const nuevovalor = viewModel.creditoModel.valorVehiculo === null ? "" :
        viewModel.creditoModel.valorVehiculo.toString() + "" + valorDigitado;

        const valor: number = parseInt(nuevovalor.replace(/\./g, ""), 0);
        const newvalor = valor.toString().replace(/(.)(?=(\d{3})+$)/g, "$1.").toString();
        const largo = newvalor.length;
        // alert(propertyChangeData.object.id);

        if (propertyChangeData.object.id === "valorVehiculo") {
            alert(viewModel.creditoModel.valorVehiculo);
            viewModel.creditoModel.valorVehiculo = newvalor;
        } else {
            viewModel.creditoModel.montoPie = newvalor;
        }
        // .object.text = newvalor;

        return false;

        // valor.toString().replace(/(.)(?=(\d{3})+$)/g, "$1.").toString();
        // propertyChangeData.object.nativeView.setSelection(largo);
    }

export function pageLoaded(args) {
    // export function onNavigatingTo(args: NavigatedData): void {
    const page = <Page>args.object;
    viewModel = new CreditosListViewModel(page.navigationContext);
    page.bindingContext = viewModel;
    console.log("load: view model");
    viewModel.load();
    /* let textField = page.getViewById("valorVehiculo");
    textField.on("textChange", textChange);
    textField = page.getViewById("montoPie");
    textField.on("textChange", textChange); */
}
