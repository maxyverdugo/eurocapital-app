import { ShownModallyData } from "tns-core-modules/ui/page";
import { View } from "ui/page";

import { CreditoDetailEditViewModel } from "./credito-edit-view-model";

/* ***********************************************************
* This is the item detail edit code behind.
* This code behind gets the selected data item, provides options to edit the item and saves the changes.
*************************************************************/
let viewModel: CreditoDetailEditViewModel;

export function onShownModally(args: ShownModallyData): void {
    viewModel = new CreditoDetailEditViewModel(args.context.credito, args.closeCallback);
    (<View>args.object).bindingContext = viewModel;
    
}


export function onCancelButtonTap(): void {
    viewModel.cancelSelection();
}