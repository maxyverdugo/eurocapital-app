import { Observable } from "data/observable";
import { Folder, knownFolders, path } from "file-system";
import { ImageSource } from "image-source";
import * as imagePicker from "nativescript-imagepicker";

import { ObservableProperty } from "../../shared/observable-property-decorator";
import { Credito } from "../shared/credito-model";
import { CreditoService } from "../shared/credito-service";
import { RoundingValueConverter } from "./roundingValueConverter";
import { VisibilityValueConverter } from "./visibilityValueConverter";
import { RootObject } from "../shared/credito-model-response";
import { ResultadoSimulacion } from "../shared/credito-model-resultado";


const tempImageFolderName = "nsimagepicker";

export class CreditoDetailEditViewModel extends Observable {
    static get imageTempFolder(): Folder {
        return knownFolders.temp().getFolder(tempImageFolderName);
    }

    private static clearImageTempFolder(): void {
        CreditoDetailEditViewModel.imageTempFolder.clear();
    }

    @ObservableProperty() credito: ResultadoSimulacion;
    @ObservableProperty() isUpdating: boolean;
    @ObservableProperty() isVisible: string = "visible";

    private _roundingValueConverter: RoundingValueConverter;
    private _visibilityValueConverter: VisibilityValueConverter;
    private _isCreditoImageDirty: boolean;
    private _creditoService: CreditoService;

    constructor(resultadoSimulacion: RootObject, private _closeCallback: Function) {
        super(); 
        
        // get a fresh editable copy of credito model
        console.log("**************************Resultado " + JSON.stringify(resultadoSimulacion.SimulacionCreditoResult.RespuestaSimulacion));
        this.credito = resultadoSimulacion.SimulacionCreditoResult.RespuestaSimulacion;
        //this._creditoService = CreditoService.getInstance();
        this.isVisible = this.credito.VFMG=="0"?"collapse":"visible";
        console.log("vmg "+this.credito.VFMG);
        console.log(this.isVisible);
        this.isUpdating = false;
        this._isCreditoImageDirty = false;

        // set up value converter to force iOS UISlider to work with discrete steps
        this._roundingValueConverter = new RoundingValueConverter();

        this._visibilityValueConverter = new VisibilityValueConverter();
    }
    cancelSelection(): void {
        this._closeCallback(null);
    }
}
