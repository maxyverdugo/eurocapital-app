
export const carClassList: Array<string> = [
    "12 Meses",
    "18 Meses",
    "24 Meses",
    "30 Meses",
    "36 Meses",
    "42 Meses",
    "48 Meses"
];

export const carClassListCI: Array<string> = [
    "24 Meses",
    "36 Meses",
]

export const carDoorList: Array<number> = [
    2,
    3,
    5
];

export const carSeatList: Array<string> = [
    "05-07-2018",
    "15-07-2018",
    "25-07-2018"
];

export const carTransmissionList: Array<string> = [
    "Manual",
    "Automatic"
];
