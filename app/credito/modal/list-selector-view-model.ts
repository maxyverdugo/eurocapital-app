import { Observable } from "data/observable";

import { carClassList, carDoorList, carSeatList, carTransmissionList, carClassListCI } from "./constants";

export class ListSelectorViewModel extends Observable {
    private _items: Array<any>;
    private _tag: string;
    private _selectedIndex: number;
    private _tipoCredito: string;

    // tslint:disable-next-line:ban-types
    constructor(context: any, private _closeCallback: Function) {
        super();

        this._tag = context.tag;
        this._tipoCredito = context.tipoCredito;
        const protoItems = this.resolveProtoItems(context.selectedValue, this._tipoCredito);
        this._selectedIndex = protoItems.indexOf(context.selectedValue);/**/
        this._items = [];
        for (let i = 0; i < protoItems.length; i++) {
            this._items.push({
                value: protoItems[i],
                isSelected: i === this._selectedIndex ? true : false
            });
        }
    }

    selectItem(newSelectedIndex: number): void {
        const oldSelectedItem = this._items[this._selectedIndex];
        oldSelectedItem.isSelected = false;

        const newSelectedItem = this._items[newSelectedIndex];
        newSelectedItem.isSelected = true;
        this._selectedIndex = newSelectedIndex;

        this._closeCallback(newSelectedItem.value);
    }

    cancelSelection(): void {
        this._closeCallback(null);
    }

    get items(): Array<any> {
        return this._items;
    }

    get title(): string {
        return 'Seleccione';// ${this.capitalizeFirstLetter(this._tag)}`;
    }

    private resolveProtoItems(value: any, tipoCredito: string): Array<any> {
        switch (this._tag) {
            case "class":
                return carClassList;
            case "doors":
                return carDoorList;
            case "seats":
                return this.listaFechas(value);
            case "transmission":
                return carTransmissionList;
        }
        if (tipoCredito == "CI") {
            return carClassListCI;
        } else {
            return carClassList;
        }
    }

    private listaFechas(value: any): Array<string> {
            let results: Array<string>;
            results = new Array();
            results.push(value);
            var today = new Date();
            today.setDate(today.getDate() + 60);
            console.log("fecha" + value);
            var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
            var dt = new Date(value.replace(pattern, '$3-$2-$1'));
            let continuar = true;
            while (continuar == true) {
                let diasMas = 5;
                if (dt.getDate() == 5 || dt.getDate() == 25) {
                    diasMas = 10;
                }
                dt.setDate(dt.getDate() + diasMas);
                if (dt > today) {
                    continuar = false;
                }
                else {
                    let dia = dt.getDate();
                    if (dia <= 5) {
                        dia = 5;
                    } else if (dia <= 15) {
                        dia = 15;
                    } else if (dia <= 20) {
                        dia = 20;
                    } else if (dia <= 25) {
                        dia = 25;
                    }

                    results.push(dia + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear());
                }
            }
            return results;
    }

    private capitalizeFirstLetter(str: string): string {
        return str;//.charAt(0).toUpperCase() + str.slice(1);
    }
}
