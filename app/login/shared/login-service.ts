import { Config } from "../../shared/config";
import { Login } from "./login-model";

import { File } from "file-system";
import { RootObject } from "./login-model-response";
const httpModule = require("http");

const editableProperties = [
    "class",
    "doors",
    "hasAC",
    "transmission",
    "luggage",
    "name",
    "price",
    "seats",
    "imageUrl"
];

export class LoginService {
    static getInstance(): LoginService {
        return LoginService._instance;
    }

    private static _instance: LoginService = new LoginService();

    private static cloneUpdateModel(login: Login): object {
        // tslint:disable-next-line:ban-comma-operator
        return editableProperties.reduce((a, e) => (a[e] = login[e], a), { _id: login.id });
    }

    private allLogins: Array<Login> = [];
    constructor() {
        if (LoginService._instance) {
            throw new Error("Use LoginService.getInstance() instead of new.");
        }

        LoginService._instance = this;
    }

    load(): Promise<any> {
        return Promise.resolve();
    }

    login(loginModel: Login): Promise<any> {
        return httpModule.request({
            url: Config.urlServices + "Login/",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                solicitaLoginModel:
                    {
                        Login:
                            {
                                Usuario: loginModel.rut,
                                Clave: loginModel.contrasena
                            }
                    }
            })
        }).then((response) => {
            const result = <RootObject>response.content.toJSON();
            console.log("respuesta: " + JSON.stringify(result));
            if (result.LoginUsuarioResult.Respuesta.Codigo === "0") {
                console.log("respuesta Correcta: " + JSON.stringify(result));
                // alert("respuesta correcta: " + JSON.stringify(result));
                loginModel.id = result.LoginUsuarioResult.Token.IdSesion;
            }

            return Promise.resolve(loginModel);

        }, (e) => {
            alert("respuesta error: " + e);

            return Promise.reject(loginModel);
        });
    }

    private getMimeType(imageExtension: string): string {
        const extension = imageExtension === "jpg" ? "jpeg" : imageExtension;

        return "image/" + extension.replace(/\./g, "");
    }
}
