import { Observable } from "data/observable";

import { ObservableProperty } from "../../shared/observable-property-decorator";

export class Login extends Observable {
    @ObservableProperty() rut: string;
    @ObservableProperty() contrasena: string;
    @ObservableProperty() _id: string;

    constructor() {
        super();
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        if (this._id === value) {
            return;
        }

        this._id = value;
        this.notifyPropertyChange("id", value);

        //this.updateDependentProperties();
    }
    /*constructor(options: any) {
        super();

        this.rut = options.rut;
        this.contrasena = options.contrasena;
        this._name = options.name;
        this._imageUrl = options.imageUrl;

        this.id = options.id;
        this.seats = options.seats;
        this.luggage = Number(options.luggage);
        this.class = options.class;
        this.hasAC = options.hasAC;
        this.doors = Number(options.doors);
        this.price = Number(options.price);
        this.transmission = options.transmission;
        this.imageStoragePath = options.imageStoragePath;

        this.updateDependentProperties();
    }*/
}
