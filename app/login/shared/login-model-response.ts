
    export class Respuesta {
        Codigo: string;
        Descripcion: string;
    }

    export class TokenResult {
        IdSesion: string;
    }

    export class LoginUsuarioResult {
        Respuesta: Respuesta;
        Token: TokenResult;
    }

    export class RootObject {
        LoginUsuarioResult: LoginUsuarioResult;
    }