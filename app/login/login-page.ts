import { topmost } from "ui/frame";
import { NavigatedData, Page } from "ui/page";

import { LoginsListViewModel } from "./login-view-model";
import { Login } from "./shared/login-model";
import { BiometricIDAvailableResult, FingerprintAuth } from "nativescript-fingerprint-auth";
import { Image } from "tns-core-modules/ui/image/image";
/* ***********************************************************
* This is the master list code behind in the master-detail structure.
* This code behind gets the data, passes it to the master view and displays it in a list.
* It also handles the navigation to the details page for each item.
*************************************************************/

/* ***********************************************************
* Use the "onNavigatingTo" handler to initialize the page binding context.
*************************************************************/
let fingerprintAuth = new FingerprintAuth();
export function onNavigatingTo(args: NavigatedData): void {
    /* ***********************************************************
    * The "onNavigatingTo" event handler lets you detect if the user navigated with a back button.
    * Skipping the re-initialization on back navigation means the user will see the
    * page in the same data state that he left it in before navigating.
    *************************************************************/
    if (args.isBackNavigation) {
        return;
    }
    const page = <Page>args.object;
    const viewModel = new LoginsListViewModel();
    page.bindingContext = viewModel;
    viewModel.load();
    // verifyAvailable();
}

export function verifyAvailable() {
    let huella = <Image>topmost().getViewById('finger');
    fingerprintAuth.available().then((result: BiometricIDAvailableResult) => {
        if (result.any == false) {
            huella.visibility = 'hidden';
        } else {
            huella.visibility = 'visible';
        }
    })
}
