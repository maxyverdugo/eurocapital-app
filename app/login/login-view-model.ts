import { Observable } from "data/observable";
import * as app from "application";
import * as appSetting from "application-settings";
import { ObservableArray } from "data/observable-array";
import { Feedback, FeedbackPosition, FeedbackType } from "nativescript-feedback";
import { topmost } from "tns-core-modules/ui/frame";
import * as dialogs from "ui/dialogs";
import { AnimationCurve } from "ui/enums";
import { Label } from "ui/label";
import { RutValidation } from "../assets/validations/rut";
import { UserSettings } from "../components/UserSettings";
import { Config } from "../shared/config";
import { ObservableProperty } from "../shared/observable-property-decorator";
import { Login } from "./shared/login-model";
import { LoginService } from "./shared/login-service";
import { BiometricIDAvailableResult, FingerprintAuth } from "nativescript-fingerprint-auth";
const platformModule = require("tns-core-modules/platform");

/* ***********************************************************
* This is the master list view model.
*************************************************************/
export class LoginsListViewModel extends Observable {
    @ObservableProperty() loginModel: Login;
    @ObservableProperty() isLoading: boolean = false;
    private _loginService: LoginService;
    private _rutValidation: RutValidation;
    private _userSettings: UserSettings;
    private feedback: Feedback;
    private fingerprintAuth: FingerprintAuth;
    private static CONFIGURED_PASSWORD = ".!K8mT_a5,_";
    private _isAvailable = false;
    constructor() {
        super();
        this.loginModel = new Login();
        this.fingerprintAuth = new FingerprintAuth();
        this.loginModel.rut = ""; // "27";
        this.loginModel.contrasena = ""; // ".!K8mT_a5,_";
        this._loginService = LoginService.getInstance();
        this._rutValidation = new RutValidation();
        this.feedback = new Feedback();
    }
    public verifyAvailable() {
        this.fingerprintAuth.available().then((result: BiometricIDAvailableResult) => {
            if (result.any == false) {
                this._isAvailable = false;
                alert({
                    title: "Información",
                    message: "Tu equipo no cuenta con lector biometrico",
                    okButtonText: "OK"
                });
            } else {
                this._isAvailable = true;
            }
        });
    }
    verifyFingerPrint(): void {
        this.verifyAvailable();
        if (this._isAvailable == true) {
            if (appSetting.getString("rut") !== undefined && appSetting.getString("contrasena") !== undefined) {
                this.fingerprintAuth
                    .verifyFingerprint({
                        title: "Escanea tu huella",
                        message: "Escanea tu huella", // optional
                        authenticationValidityDuration: 10, // Android
                        useCustomAndroidUI: true
                    })
                    .then(() => {
                        this.loginModel.rut = appSetting.getString("rut");
                        this.loginModel.contrasena = appSetting.getString("contrasena");
                        this.isLoading = true;
                        this._loginService.login(this.loginModel)
                            .then((user: Login) => {
                                console.log("user: " + JSON.stringify(user));
                                this.isLoading = false;
                                if (user.id != null && user.id !== "") {
                                    this.navigateHome(user);
                                    // console.log("user: " + JSON.stringify(user));
                                    console.log("datos app: " + appSetting.getString("rut") + " " + appSetting.getString("contrasena"));
                                } else {
                                    if (platformModule.isIOS) {
                                        this.feedback.warning({
                                            message: "Credenciales ingresadas son inválidas."
                                        });
                                    } else {
                                        dialogs.alert({
                                            title: "Error",
                                            message: "Credenciales ingresadas son inválidas.",
                                            okButtonText: "Aceptar"
                                        }).then(() => {
                                            console.log("Dialog closed!");
                                        });
                                    }
                                }
                            })
                            .catch((error) => {
                                console.log("error en login");
                                this.isLoading = false;
                                if (platformModule.isIOS) {
                                    this.feedback.warning({
                                        message: "Error de conexión al sistema de login."
                                    });
                                } else {
                                    dialogs.alert({
                                        title: "Error",
                                        message: "Error de conexión al sistema de login.",
                                        okButtonText: "Aceptar"
                                    }).then(() => {
                                        console.log("Dialog closed!");
                                    });
                                }
                            });

                    })
                    .catch(err => {
                        alert({
                            title: "Lectura de huella erronea",
                            message: JSON.stringify(err),
                            okButtonText: "OK"
                        });
                    });
            } else {
                alert({
                    title: "Error",
                    message: "Para utilizar tu huella digital, debes inicar sesión por primera vez",
                    okButtonText: "OK"
                });
            }
        }
    }
    tap(args): void {
        console.log("tap label ");
        const label: Label = <Label>args.object.page.getViewById("labelid");
        console.log("label " + label + " " + args.object.page);
        label.animate({
            translate: { x: 0, y: -20 },
            duration: 1000,
            curve: AnimationCurve.cubicBezier(0.1, 0.1, 0.1, 1)
        });
    }
    registrar() {
        topmost().navigate({
            moduleName: "/registro/registro-page",
            context: null,
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }
    onReturnPress() {
        // this.login();
    }

    login() {
        if (this.loginModel.rut === "" || this.loginModel.rut == null) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar un Rut",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar un Rut",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }

            return;

        }
        if (!this._rutValidation.validar(this.loginModel.rut)) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar un Rut válido",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar un Rut válido",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }

            return;
        }
        if (this.loginModel.contrasena === "") {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar una Contraseña",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar una Contraseña",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }

            return;
        }
        console.log("user: " + topmost());
        this.isLoading = true;
        this._loginService.login(this.loginModel)
            .then((user: Login) => {
                console.log("user: " + JSON.stringify(user));
                this.isLoading = false;
                if (user.id != null && user.id !== "") {
                    appSetting.setString("rut", user.rut);
                    appSetting.setString("contrasena", user.contrasena);
                    this.navigateHome(user);
                    // console.log("user: " + JSON.stringify(user));
                    console.log("datos app: " + appSetting.getString("rut") + " " + appSetting.getString("contrasena"));
                } else {
                    if (platformModule.isIOS) {
                        this.feedback.warning({
                            message: "Credenciales ingresadas son inválidas."
                        });
                    } else {
                        dialogs.alert({
                            title: "Error",
                            message: "Credenciales ingresadas son inválidas.",
                            okButtonText: "Aceptar"
                        }).then(() => {
                            console.log("Dialog closed!");
                        });
                    }
                }
            })
            .catch((error) => {
                console.log("error en login");
                this.isLoading = false;
                if (platformModule.isIOS) {
                    this.feedback.warning({
                        message: "Error de conexión al sistema de login."
                    });
                } else {
                    dialogs.alert({
                        title: "Error",
                        message: "Error de conexión al sistema de login.",
                        okButtonText: "Aceptar"
                    }).then(() => {
                        console.log("Dialog closed!");
                    });
                }
            });
    }

    load(): void {
        this.isLoading = false;
    }

    private navigateHome(user: Login) {
        topmost().navigate({
            moduleName: "/credito/credito-page",
            context: user,
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }
}
