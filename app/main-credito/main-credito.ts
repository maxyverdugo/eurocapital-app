import * as app from "application";
import { EventData } from "data/observable";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { isIOS } from "tns-core-modules/platform";
import { NavigatedData, Page } from "ui/page";
import { CreditosListViewModel } from "../credito/credito-view-model";
import { MainCreditoViewModel } from "./main-credito-view-model";

/*export function onLoaded(args: EventData): void {
    alert("entro");
    const drawerComponent = <RadSideDrawer>args.object;
    viewModel = new CreditosListViewModel(drawerComponent.page.navigationContext);
    drawerComponent.bindingContext = viewModel; // new MainCreditoViewModel();
}
*/

export function drawerLoaded(args: EventData) {
    const drawer = <RadSideDrawer>args.object;

    if (isIOS) {
        drawer.ios.defaultSideDrawer.style.shadowMode = 2; // TKSideDrawerShadowMode.SideDrawer;
        drawer.ios.defaultSideDrawer.style.dimOpacity = 0.12;
        drawer.ios.defaultSideDrawer.style.shadowOpacity = 0.75; // 0-1, higher is darker
        drawer.ios.defaultSideDrawer.style.shadowRadius = 5; // higher is more spread
        drawer.ios.defaultSideDrawer.transitionDuration = 0.25;
    }

    drawer.bindingContext = new MainCreditoViewModel(); // CreditosListViewModel(drawer.page.navigationContext);
}
