export class Format {
    constructor() {
    }
    currency(number: number): string
    {
        return "$" + number.toFixed(0).replace(/./g, function(c, i, a) {
            return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
        });
    }
}