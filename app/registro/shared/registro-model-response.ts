
    export class Respuesta {
        Codigo: string;
        Descripcion: string;
    }

    export class RegistrarResult {
        Respuesta: Respuesta;
    }

    export class RootObject {
        RegistrarResult: RegistrarResult;
    }