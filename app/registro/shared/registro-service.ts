import { Config } from "../../shared/config";
import { Registro } from "./registro-model";

import { File } from "file-system";
import { RootObject } from "./registro-model-response";
const httpModule = require("http");

const editableProperties = [
    "class",
    "doors",
    "hasAC",
    "transmission",
    "luggage",
    "name",
    "price",
    "seats",
    "imageUrl"
];

export class RegistroService {
    static getInstance(): RegistroService {
        return RegistroService._instance;
    }

    private static _instance: RegistroService = new RegistroService();

    private static cloneUpdateModel(registro: Registro): object {
        // tslint:disable-next-line:ban-comma-operator
        return editableProperties.reduce((a, e) => (a[e] = registro[e], a), { _id: registro.id });
    }

    constructor() {
        if (RegistroService._instance) {
            throw new Error("Use RegistroService.getInstance() instead of new.");
        }

        RegistroService._instance = this;
    }

    load(): Promise<any> {
        return Promise.resolve();
    }

    registrar(registroModel: Registro): Promise<any> {
        return httpModule.request({
            url: Config.urlServices + "Registrar/",
            method: "POST",
            headers: { "Content-Type": "application/json" },
            content: JSON.stringify({
                        registrarModel:
                            {
                                RutUsuario: registroModel.rut,
                                EmailUsuario: registroModel.email
                            }
            })
        }).then((response) => {
            const result = <RootObject>response.content.toJSON();
            console.log("respuesta: " + JSON.stringify(result));
            if (result.RegistrarResult.Respuesta.Codigo === "0") {
                console.log("respuesta Correcta: " + JSON.stringify(result));
                registroModel.id = result.RegistrarResult.Respuesta.Codigo;
            }

            return Promise.resolve(registroModel);

        }, (e) => {

            return Promise.reject(registroModel);
        });
    }

    private getMimeType(imageExtension: string): string {
        const extension = imageExtension === "jpg" ? "jpeg" : imageExtension;

        return "image/" + extension.replace(/\./g, "");
    }
}
