import { Observable } from "data/observable";
import { ObservableArray } from "data/observable-array";
import { Feedback, FeedbackPosition, FeedbackType } from "nativescript-feedback";
import { topmost } from "tns-core-modules/ui/frame";
import * as dialogs from "ui/dialogs";
import { AnimationCurve } from "ui/enums";
import { Label } from "ui/label";
import { RutValidation } from "../assets/validations/rut";
import { UserSettings } from "../components/UserSettings";
import { Config } from "../shared/config";
import { ObservableProperty } from "../shared/observable-property-decorator";
import { Registro } from "./shared/registro-model";
import { RegistroService } from "./shared/registro-service";
const platformModule = require("tns-core-modules/platform");

/* ***********************************************************
* This is the master list view model.
*************************************************************/
export class RegistroViewModel extends Observable {
    @ObservableProperty() registroModel: Registro;
    @ObservableProperty() isLoading: boolean = false;
    private _registroService: RegistroService;
    private _rutValidation: RutValidation;
    private _userSettings: UserSettings;
    private feedback: Feedback;
    constructor() {
        super();
        this.registroModel = new Registro();
        this.registroModel.rut = "";
        this.registroModel.email = "";
        this._registroService = RegistroService.getInstance();
        this._rutValidation = new RutValidation();
        this.feedback = new Feedback();
    }
    tap(args): void {
        console.log("tap label ");
        const label: Label = <Label> args.object.page.getViewById("labelid");
        console.log("label " + label + " " + args.object.page);
        label.animate({
            translate: { x: 0, y: -20 },
            duration: 1000,
            curve: AnimationCurve.cubicBezier(0.1, 0.1, 0.1, 1)
        });
    }

    volver() {
        topmost().navigate({
            moduleName: "/login/login-page",
            context: null,
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }
    solicitar() {
        if (this.registroModel.rut === "" || this.registroModel.rut == null) {
            if (platformModule.isIOS) {
                this.feedback.error({
                    message: "Debes ingresar un Rut",
                    position: FeedbackPosition.Bottom // iOS only
                });
            } else {
                dialogs.alert({
                    title: "Error",
                    message: "Debes ingresar un Rut",
                    okButtonText: "Aceptar"
                }).then(() => {
                    console.log("Dialog closed!");
                });
            }

            return;

        }
        if (!this._rutValidation.validar(this.registroModel.rut)) {
            if (platformModule.isIOS) {
            this.feedback.error({
                message: "Debes ingresar un Rut válido",
                position: FeedbackPosition.Bottom // iOS only
            });
        } else {
            dialogs.alert({
                title: "Error",
                message: "Debes ingresar un Rut válido",
                okButtonText: "Aceptar"
            }).then(() => {
                console.log("Dialog closed!");
            });
        }

            return;
        }
        if (this.registroModel.email === "") {
            if (platformModule.isIOS) {
            this.feedback.error({
                message: "Debes ingresar un Email",
                position: FeedbackPosition.Bottom // iOS only
            });
        } else {
            dialogs.alert({
                title: "Error",
                message: "Debes ingresar un Email",
                okButtonText: "Aceptar"
            }).then(() => {
                console.log("Dialog closed!");
            });
        }

            return;
        }
        console.log("user: " + topmost());
        this.isLoading = true;
        this._registroService.registrar(this.registroModel)
                .then((user: Registro) => {
                    this.isLoading = false;
                    if (user.id != null && user.id !== "") {
                       // this._userSettings = UserSettings.getInstance();
                       // let result = this._userSettings.guardar(this.loginModel.rut);
                       if (platformModule.isIOS) {
                        this.feedback.success({
message: "La solicitud de alta ha sido enviada correctamente. Nos pondremos en contacto con usted."
                        });
                    } else {
dialogs.alert({
title: "Confirmación",
message: "La solicitud de alta ha sido enviada correctamente. Nos pondremos en contacto con usted.",
okButtonText: "Aceptar"
                        }).then(() => {
                            console.log("Dialog closed!");
                        });
                    }
                       this.navigateHome(user);
                       console.log("user: " + JSON.stringify(user));
                    } else {
                        if (platformModule.isIOS) {
                        this.feedback.warning({
                            message: "La solicitud de alta no se ha podido procesar."
                        });
                    } else {
                        dialogs.alert({
                        title: "Error",
                        message: "La solicitud de alta no se ha podido procesar.",
                        okButtonText: "Aceptar"
                                                }).then(() => {
                                                    console.log("Dialog closed!");
                                                });
                                            }
                    }
                })
                .catch((error) => {
                    this.isLoading = false;
                    if (platformModule.isIOS) {
                    this.feedback.warning({
                        message: "Error de conexión al sistema de registro."
                    });
                } else {
                    dialogs.alert({
                        title: "Error",
                        message: "Error de conexión al sistema de registro.",
                        okButtonText: "Aceptar"
                                                }).then(() => {
                                                    console.log("Dialog closed!");
                                                });
                }
                });

    }

    load(): void {
        this.isLoading = false;
    }

    private navigateHome(user: Registro) {
        topmost().navigate({
            moduleName: "/login/login-page",
            context: null,
            animated: true,
            transition: {
                name: "slideLeft",
                duration: 350,
                curve: "ease"
            }
        });
    }
}
