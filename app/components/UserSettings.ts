let sqlLiteModule= require("nativescript-sqlite");
var ObservableArray = require("data/observable-array").ObservableArray;
import { Models } from '../models/Models';

export class UserSettings {
    private data = new ObservableArray();
   private DATABASE_NAME = 'eurocapital-db.sqlite';
   private USER_SETTINGS_DOC_ID = 'usersettings';
   private _database;

   private static _instance :UserSettings =null;
 private db = null;
    private constructor() {
    }
    public inicializa():boolean
    {
        let result:boolean = false;
        if (!sqlLiteModule.exists(this.DATABASE_NAME)) {
            //sqlLiteModule.copyDatabase(this.DATABASE_NAME);
            new sqlLiteModule(this.DATABASE_NAME).then(db => {
                db.execSQL("CREATE TABLE IF NOT EXISTS usuario (id varchar(15) PRIMARY KEY, nombre varchar(75))").then(id => {
                }, error => {
                    console.log("CREATE TABLE ERROR", error);
                });
            }, error => {
                 
            });
        }

          new sqlLiteModule(this.DATABASE_NAME, {key: 'testing'}, function(err, dbConnection) {
            if (err) {
                console.log(err, err.stack);
            }
            dbConnection.resultType(sqlLiteModule.RESULTSASOBJECT);
    
            dbConnection.version().then(function (results) {
                console.log("User Version: ", results, typeof results, Number.isInteger(results)); //, String.isString(results));
            }); 
            if (sqlLiteModule.HAS_ENCRYPTION) {
                dbConnection.get("PRAGMA cipher_version;").then(function(results) {
                    console.log("Cipher version", results['cipher_version']);
                });
            }

            dbConnection.resultType(sqlLiteModule.RESULTSASOBJECT);
            dbConnection.valueType(sqlLiteModule.VALUESARENATIVE);
        
            dbConnection.all('select id from usuario', function (err, loadedData) {
                this.data.length = 0;
                if (err) {
                    console.log(err);
                } else {
                    console.log('select id from usuario');
                    if(loadedData.length>0){
                        result=true;
                    }
                    /*for (var i=0;i<loadedData.length;i++) {
                        if (i % 2 === 0) {
                            loadedData[i].css = "one";
                        } else {
                            loadedData[i].css = "two";
                        }
                        this.data.push(loadedData[i]);
                    }*/
                }
            });
            //reloadData();
            console.log("database xxxx " + dbConnection);
            return result;
        }).catch(function(val) {
            console.log("sqlite error", val);
        });

            
           //this._userSettingsDocument = this._database.getDocument(this.USER_SETTINGS_DOC_ID);
           /*if (!this._userSettingsDocument) {
                console.log("Document does not exist yet :)");
                this._userSettingsObj = {
                    username: "",
                }
                this._database.createDocument(this._userSettingsObj, this.USER_SETTINGS_DOC_ID);
                this._userSettingsDocument = this._database.getDocument(this.USER_SETTINGS_DOC_ID);
           } */
    return result;
    }

    public guardar(rut:string):boolean
    {
        let result:boolean = false;
        new sqlLiteModule(this.DATABASE_NAME, {key: 'testing'}, function(err, dbConnection) {
            if (err) {
                console.log('error conect ' + err, err.stack);
            }
            console.log('Conectado ');
            dbConnection.all('select id from usuario', function (err, loadedData) {
                this.data.length = 0;
                console.log("Paho por aqui");
                if (err) {
                    console.log("Error xxxx " + err);
                } else {
                    console.log('select id from usuario');
                    if(loadedData.length==0){
                        result=true;
                        //UPDATE
                        dbConnection.execSQL(
                            "INSERT INTO usuario (id, nombre) VALUES (?, ?)", 
                            [rut, '']
                        ).then((id) => {
                        }, (e) => {
                        });
                    } else {
                        console.log("UPDATE xxxx " + dbConnection);
                    }
                }
            }).catch(function(val) {
                console.log("sqlite interior error", val);
            });
            //reloadData();
            console.log("database respuesta " + dbConnection);
            return result;
        }).catch(function(val) {
            console.log("sqlite error", val);
        });

            
           //this._userSettingsDocument = this._database.getDocument(this.USER_SETTINGS_DOC_ID);
           /*if (!this._userSettingsDocument) {
                console.log("Document does not exist yet :)");
                this._userSettingsObj = {
                    username: "",
                }
                this._database.createDocument(this._userSettingsObj, this.USER_SETTINGS_DOC_ID);
                this._userSettingsDocument = this._database.getDocument(this.USER_SETTINGS_DOC_ID);
           } */
    return result;
    }

    public static getInstance() :UserSettings{

       if(!this._instance){
              this._instance = new UserSettings();
                
        }
    
    return this._instance;
    
    }
}