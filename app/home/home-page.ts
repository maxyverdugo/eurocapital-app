import * as application from "application";
import { EventData } from 'data/observable';
import { StackLayout } from 'ui/layouts/stack-layout';
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { AppRootViewModel } from "../app-root/app-root-view-model";

export function pageLoaded(args: EventData) {

}

export function onOpenDrawerTap() {
    const sideDrawer = <RadSideDrawer>application.getRootView();
    sideDrawer.toggleDrawerState();
}